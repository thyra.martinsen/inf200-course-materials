"""
Example solution for "tail" task.

This code contains several different implementations of a tail()
function printing the last n lines of a file.
"""

__author__ = 'Hans Ekkehard Plesser, NMBU'


import os
import time


def tail_read(filename, n=5):
    """
    Print last lines of a file.

    This implementation reads the entire file at once.

    :param filename: Name of file to display
    :param n: Number of lines to show, default 5
    """

    # Nothing to do if n < 1
    if n < 1:
        return

    text = open(filename).read()

    # rstrip() to remove newline after last line in file, if present
    lines = text.rstrip('\n').split('\n')

    # We use Python's negative indexing logic:
    # lines[-2:] gives the last two lines.
    # It is important that we do not get here if n < 1,
    # because then num_to_show would be >= 0 and we would
    # index from the beginning instead of the end.
    # The if n < 1: return above ensures this.
    num_to_show = max(-n, -len(lines))
    for line in lines[num_to_show:]:
        print(line)


def tail_by_line(filename, n=5):
    """
    Print last lines of a file.

    This implementation reads the file line by line and
    keeps the last n lines read in a list.

    :param filename: Name of file to display
    :param n: Number of lines to show, default 5
    """

    # We do not know in advance which line is the n-th last line.
    # Therefore, we buffer the last n lines in a list.
    lines = []
    with open(filename) as infile:
        while line := infile.readline():
            lines.append(line)    # append at end
            if len(lines) > n:
                lines.pop(0)      # drop at beginning of list if more than n

    # We now have the final n lines of the file in lines and can print
    for line in lines:
        print(line.rstrip('\n'))


def tail_by_seek(filename, n=5):
    """
    Print last lines of a file.

    This implementation reads bytewise from the end of the file
    until it has read the required number of newlines.

    A single newline as the last byte of a file is ignored.

    NOTE: Needs more checking of corner cases.

    :param filename: Name of file to display
    :param n: Number of lines to show, default 5
    """

    # Open in binary mode to be able to seek
    with open(filename, 'rb') as infile:
        # If last byte is newline, ignore it. If it is not, it
        # does not matter, so we can always start at -2
        infile.seek(-2, os.SEEK_END)
        while n > 0 and (c := infile.read(1)):
            if c == b'\n':
                n -= 1   # count down newlines seen

            if infile.tell() > 1:
                # -2: one for c we just read and one to get one to the left
                infile.seek(-2, os.SEEK_CUR)
            else:
                infile.seek(0, os.SEEK_SET)
                break
        else:
            infile.seek(2, os.SEEK_CUR)

        # We have moved pos to one left of last newline to skip.
        # Move two to right to be at beginning of first line to print.
        for line in infile:
            print(line.decode('utf-8').rstrip('\n'))


if __name__ == '__main__':
    # Run all variants, printing the last six lines of this code
    for tail in [tail_read, tail_by_line, tail_by_seek]:
        print(42 * '-')
        print('Implementation:', tail.__name__)
        print(42 * '-')

        s = time.time()
        tail('tail.py', 6)
        d = time.time() - s

        print(42 * '-')
        print('Duration:', d)
        print(42 * '=')

        # Extra test cases with large files
        #tail('/Users/plesser/Courses/INF120/V2019/pwned-passwords-sha1-ordered-by-hash-v4-1000th.txt')
        #tail('/Users/plesser/Courses/INF221/pwned-passwords-sha1-ordered-by-hash-v5.txt')
