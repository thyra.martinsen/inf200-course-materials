"""
Example solution to INF200 Week 37 Task 1.

This program reads a CSV file with population data for Norwegian
municipalities and prints a table of district populations in
descending order.

FCW (flexible column width):
This version uses variables to store column widths
instead of hard-coding them in format strings.
"""

__author__ = "Hans Ekkehard Plesser, NMBU"

# Part 1: Collect data from file
district_population = {}
with open('norway_municipalities_2017.csv', 'r') as infile:
    header = infile.readline().strip().split(',')
    for line in infile:
        _, district, population = line.split(',')
        district_population[district] = district_population.get(district, 0) + int(population)

# Part 2: Print table
NAME_WIDTH = 20
COUNT_WIDTH = 10
TABLE_WIDTH = NAME_WIDTH + COUNT_WIDTH

print('-' * TABLE_WIDTH)
print(f'{header[1]:{NAME_WIDTH}s}{header[2]:>{COUNT_WIDTH}s}')
print('-' * TABLE_WIDTH)

for district, population in sorted(district_population.items(),
                                   key=lambda s: s[1],
                                   reverse=True):
    print(f'{district:{NAME_WIDTH}s}{population:{COUNT_WIDTH}d}')

print('-' * TABLE_WIDTH)
