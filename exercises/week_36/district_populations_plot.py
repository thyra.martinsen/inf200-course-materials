"""
Plot district populations.

Hans Ekkehard Plesser, NMBU
"""

import pandas as pd
import matplotlib.pyplot as plt

pd.read_csv("norway_municipalities_2017.csv").groupby('District').sum().plot(kind='bar')
plt.show()
